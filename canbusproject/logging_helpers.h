 
#include <SparkFun_RV8803.h>
#include <SD.h>

#define PIN_LIGHT_DATA_LOG_ALERT 6

RV8803 sparkfunRTCModule;
String LogFileName;
uint32_t sdCardSize;
uint32_t spaceRemainingOnSdCardAtStart;
uint32_t totalDataWrittenToSDCard = 0;


double PREVIOUSLY_RECORDED_VEHICLE_DATA[0xC4][2];
double PREVIOUSLY_RECORDED_SENSOR_DATA[20];

void logArbitraryData(String field, double value)
{
  sparkfunRTCModule.updateTime();
  
  digitalWrite(PIN_LIGHT_DATA_LOG_ALERT, LOW);
  File dataFile = SD.open(LogFileName, FILE_WRITE);
  dataFile.print(sparkfunRTCModule.getEpoch());
  dataFile.print("::");
  dataFile.print(field);
  dataFile.print("::");
  dataFile.println(value);
  dataFile.close();

  totalDataWrittenToSDCard += sizeof(sparkfunRTCModule.getEpoch()) + 2 + field.length() + 2 + sizeof(value);

  digitalWrite(PIN_LIGHT_DATA_LOG_ALERT, HIGH);
}

bool logSensorData(String field, double previousRecordedValue, double proposedNewValue, double variationDown, double variationUp)
{
  bool shouldLog = false;
  if (proposedNewValue < previousRecordedValue * (1 - variationUp))
    shouldLog = true;
  if (proposedNewValue > previousRecordedValue * (1 + variationDown))
    shouldLog = true;

  if (!shouldLog)
    return false;

  logArbitraryData(field, proposedNewValue);
}

bool logVehicleData(byte PID, int parameter, double proposedNewValue)
{
  bool shouldLog = false;
  PIDInfo targetPID = getPIDInfoFromHexValue(PID);
  PIDParameters targetParameter = targetPID.Parameters[parameter];

  if (proposedNewValue < PREVIOUSLY_RECORDED_VEHICLE_DATA[PID][parameter] * (1 - targetParameter.VariationDown))
    shouldLog = true;
  if (proposedNewValue > PREVIOUSLY_RECORDED_VEHICLE_DATA[PID][parameter] * (1 + targetParameter.VariationUp))
    shouldLog = true;

  if (!shouldLog)
    return false;

  logArbitraryData("PID_" + String(PID), proposedNewValue);
}
