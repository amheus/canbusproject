#include <stdint.h>
#include "dataclasses.h"
#include "arithmetic_helpers.h"

double GenericParser(const char* formula, long values[])
{
  return evaluateFormula(formula, values);
}
double O2TrimParser(const char* formula, long values[])
{
  if(String(values[1], HEX) == "FF")
  {
    return evaluateFormula(formula, {values[0]});
  }
  return evaluateFormula(formula, values);
}

const PROGMEM BitEncodedPIDInfo BIT_ENCODED_CURRENT_DATA_PIDS[] = {
  // {0x01, {{0x01, "test"}}},
  // {0x02},
  {0x03, { // Fuel system status
    {0x00, "1234567890123456"},
    {0x01, "Open:EngTmempLow"},
    {0x02, "Close"},
    {0x04, "Open:EngineLoad"},
    {0x08, "Open:SysFailure"},
    {0x16, "Close:!!FAULT!!"}
  }},
  {0x12, { // Commanded secondary air status
    {0x01, "Upstream"},
    {0x02, "Downstream"},
    {0x04, "Outside"},
    {0x08, "Diag"}
  }},
  // 0x1C
  // 0x51
};

const PROGMEM PIDInfo CURRENT_DATA_PIDS[] = {
  
  {0x00,  0,  0,  { } }, // 0x00 {get supported PIDs}
  //////////////////////////////////////////////////
  // PID GROUP 1/7 {0x01-0x20}
  //////////////////////////////////////////////////
  {0x01,  0,  0,  { } },
  {0x02,  0,  0,  { } },
  {0x03,  0,  0,  { } },
  {0x04,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 },  } },  // Calculated engine   load
  {0x05,  1,  1,  { {  -40,   215,         GenericParser,   "A-40",                 "°C",           0.1,0.1 },  } },  // Engine     coolant  temperature
  {0x06,  1,  1,  { {  -100,  99.2,        GenericParser,   "(100/128)*A-100",      "%",            0.1,0.1 },  } },  // Short      term     fuel     trim—Bank 1
  {0x07,  1,  1,  { {  -100,  99.2,        GenericParser,   "(100/128)*A-100",      "%",            0.1,0.1 },  } },  // Long       term     fuel     trim—Bank 1
  {0x08,  1,  1,  { {  -100,  99.2,        GenericParser,   "(100/128)*A-100",      "%",            0.1,0.1 },  } },  // Short      term     fuel     trim—Bank 2
  {0x09,  1,  1,  { {  -100,  99.2,        GenericParser,   "(100/128)*A-100",      "%",            0.1,0.1 },  } },  // Long       term     fuel     trim—Bank 2
  {0x0A,  1,  1,  { {  0,     765,         GenericParser,   "3*A",                  "kPa",          0.1,0.1 },  } },  // Fuel       pressure {gauge   pressure}
  {0x0B,  1,  1,  { {  0,     255,         GenericParser,   "A",                    "kPa",          0.1,0.1 },  } },  // Intake     manifold absolute pressure
  {0x0C,  2,  1,  { {  0,     16383.75,    GenericParser,   "(256*A+B)/4",          "rpm",          0.1,0.1 },  } },  // Engine     speed
  {0x0D,  1,  1,  { {  0,     255,         GenericParser,   "A",                    "km/h",         0.1,0.1 },  } },  // Vehicle    speed
  {0x0E,  1,  1,  { {  -64,   64,          GenericParser,   "(A/2)-64",             "° before TDC", 0.1,0.1 },  } },  // Timing   advance
  {0x0F,  1,  1,  { {  -40,   215,         GenericParser,   "A-40",                 "°C",           0.1,0.1 },  } },  // Intake     air      temperature
  {0x10,  1,  1,  { {  0,     655,         GenericParser,   "(256*A+B)/100",        "g/s",          0.1,0.1 },  } },  // Mass       air      flow     sensor    {MAF} air flow rate
  {0x11,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 },  } },  // Throttle   position
  {0x12,  0,  0,  { } },
  {0x13,  0,  0,  { } },
  {0x14,  2,  2,  { {  0,     1.275,       GenericParser,   "A/200",                "V",            0.1,0.1 },  { -100,   99.2,       O2TrimParser,     "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Oxygen Sensor 1 A  : Voltage B                                  : Short term fuel trim
  {0x15,  2,  2,  { {  0,     1.275,       GenericParser,   "A/200",                "V",            0.1,0.1 },  { -100,   99.2,       O2TrimParser,     "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Oxygen Sensor 2 A  : Voltage B                                  : Short term fuel trim
  {0x16,  2,  2,  { {  0,     1.275,       GenericParser,   "A/200",                "V",            0.1,0.1 },  { -100,   99.2,       O2TrimParser,     "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Oxygen Sensor 3 A  : Voltage B                                  : Short term fuel trim
  {0x17,  2,  2,  { {  0,     1.275,       GenericParser,   "A/200",                "V",            0.1,0.1 },  { -100,   99.2,       O2TrimParser,     "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Oxygen Sensor 4 A  : Voltage B                                  : Short term fuel trim
  {0x18,  2,  2,  { {  0,     1.275,       GenericParser,   "A/200",                "V",            0.1,0.1 },  { -100,   99.2,       O2TrimParser,     "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Oxygen Sensor 5 A  : Voltage B                                  : Short term fuel trim
  {0x19,  2,  2,  { {  0,     1.275,       GenericParser,   "A/200",                "V",            0.1,0.1 },  { -100,   99.2,       O2TrimParser,     "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Oxygen Sensor 6 A  : Voltage B                                  : Short term fuel trim
  {0x1A,  2,  2,  { {  0,     1.275,       GenericParser,   "A/200",                "V",            0.1,0.1 },  { -100,   99.2,       O2TrimParser,     "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Oxygen Sensor 7 A  : Voltage B                                  : Short term fuel trim
  {0x1B,  2,  2,  { {  0,     1.275,       GenericParser,   "A/200",                "V",            0.1,0.1 },  { -100,   99.2,       O2TrimParser,     "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Oxygen Sensor 8 A  : Voltage B                                  : Short term fuel trim
  {0x1C,  0,  0,  { } },
  {0x1D,  0,  0,  { } },
  {0x1E,  0,  0,  { } },
  {0x1F,  2,  1,  { {  0,     65535,       GenericParser,   "256*A+B",              "s",            0.1,0.1 }, } }, // Run time since engine start
  {0x20,  0,  0,  { } },
  //////////////////////////////////////////////////
  // PID GROUP 2/7 {0x21-0x40}
  //////////////////////////////////////////////////
  {0x21,  2,  1,  { {  0,     65535,       GenericParser,   "256*A+B",              "km",           0.1,0.1 }, } }, // Distance traveled with malfunction indicator lamp {MIL} on
  {0x22,  2,  1,  { {  0,     5177.265,    GenericParser,   "0.079*(256*A+B)",      "kPa",          0.1,0.1 }, } }, // Fuel Rail Pressure {relative to manifold vacuum}
  {0x23,  2,  1,  { {  0,     655350,      GenericParser,   "10*(256*A+B)",         "kPa",          0.1,0.1 }, } }, // Fuel Rail Gauge Pressure {diesel, or gasoline direct injection}
  {0x24,  2,  2,  { {  0,     2,           GenericParser,   "(2/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 1 || AB: Air-Fuel Equivalence Ratio {lambda,λ} || CD: Voltage
  {0x25,  2,  2,  { {  0,     2,           GenericParser,   "(2/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 2 || AB: Air-Fuel Equivalence Ratio {lambda,λ} || CD: Voltage
  {0x26,  2,  2,  { {  0,     2,           GenericParser,   "(2/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 3 || AB: Air-Fuel Equivalence Ratio {lambda,λ} || CD: Voltage
  {0x27,  2,  2,  { {  0,     2,           GenericParser,   "(2/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 4 || AB: Air-Fuel Equivalence Ratio {lambda,λ} || CD: Voltage
  {0x28,  2,  2,  { {  0,     2,           GenericParser,   "(2/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 5 || AB: Air-Fuel Equivalence Ratio {lambda,λ} || CD: Voltage
  {0x29,  2,  2,  { {  0,     2,           GenericParser,   "(2/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 6 || AB: Air-Fuel Equivalence Ratio {lambda,λ} || CD: Voltage
  {0x2A,  2,  2,  { {  0,     2,           GenericParser,   "(2/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 7 || AB: Air-Fuel Equivalence Ratio {lambda,λ} || CD: Voltage
  {0x2B,  2,  2,  { {  0,     2,           GenericParser,   "(2/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 8 || AB: Air-Fuel Equivalence Ratio {lambda,λ} || CD: Voltage
  {0x2C,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Commanded EGR {Exhaust gas recirculation}
  {0x2D,  1,  1,  { { -100,   99.2,        GenericParser,   "(100/128)*A-100",      "%",            0.1,0.1 }, } }, // EGR Error
  {0x2E,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Commanded evaporative purge
  {0x2F,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Fuel Tank Level Input
  {0x30,  1,  1,  { {  0,     255,         GenericParser,   "A",                    "",             0.1,0.1 }, } }, // Warm-ups since codes cleared
  {0x31,  2,  1,  { {  0,     65535,       GenericParser,   "256*A+B",              "km",           0.1,0.1 }, } }, // Distance traveled since codes cleared
  {0x32,  2,  1,  { { -8192,  8191.75,     GenericParser,   "(256*A+B)/4",          "Pa",           0.1,0.1 }, } }, // Evap. System Vapor Pressure !!!!!AB is two's complement signed!!!!!
  {0x33,  1,  1,  { {  0,     255,         GenericParser,   "A",                    "kPa",          0.1,0.1 }, } }, // Absolute Barometric Pressure
  {0x34,  2,  2,  { {  0,     2,           GenericParser,   "(6/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 1 || AB                                    : Air-Fuel Equivalence Ratio {lambda,λ} || CD: Current
  {0x35,  2,  2,  { {  0,     2,           GenericParser,   "(6/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 2 || AB                                    : Air-Fuel Equivalence Ratio {lambda,λ} || CD: Current
  {0x36,  2,  2,  { {  0,     2,           GenericParser,   "(6/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 3 || AB                                    : Air-Fuel Equivalence Ratio {lambda,λ} || CD: Current
  {0x37,  2,  2,  { {  0,     2,           GenericParser,   "(6/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 4 || AB                                    : Air-Fuel Equivalence Ratio {lambda,λ} || CD: Current
  {0x38,  2,  2,  { {  0,     2,           GenericParser,   "(6/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 5 || AB                                    : Air-Fuel Equivalence Ratio {lambda,λ} || CD: Current
  {0x39,  2,  2,  { {  0,     2,           GenericParser,   "(6/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 6 || AB                                    : Air-Fuel Equivalence Ratio {lambda,λ} || CD: Current
  {0x3A,  2,  2,  { {  0,     2,           GenericParser,   "(6/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 7 || AB                                    : Air-Fuel Equivalence Ratio {lambda,λ} || CD: Current
  {0x3B,  2,  2,  { {  0,     2,           GenericParser,   "(6/65536)*(256*A+B)",  "ratio",        0.1,0.1 },  {  0,     8,          GenericParser,    "(8/65536)*(256*C+D)",    "V",            0.1,0.1 },  } }, // Oxygen Sensor 8 || AB                                    : Air-Fuel Equivalence Ratio {lambda,λ} || CD: Current
  {0x3C,  2,  1,  { { -40,    6513.5,      GenericParser,   "((256*A+B)/10)-40",    "",             0.1,0.1 }, } }, // Catalyst Temperature : Bank 1, Sensor 1
  {0x3D,  2,  1,  { { -40,    6513.5,      GenericParser,   "((256*A+B)/10)-40",    "",             0.1,0.1 }, } }, // Catalyst Temperature : Bank 2, Sensor 1
  {0x3E,  2,  1,  { { -40,    6513.5,      GenericParser,   "((256*A+B)/10)-40",    "",             0.1,0.1 }, } }, // Catalyst Temperature : Bank 1, Sensor 2
  {0x3F,  2,  1,  { { -40,    6513.5,      GenericParser,   "((256*A+B)/10)-40",    "",             0.1,0.1 }, } }, // Catalyst Temperature : Bank 2, Sensor 2
  {0x40,  0,  0,  { } },
  //////////////////////////////////////////////////
  // PID GROUP 3/7 {0x41-0x60}
  //////////////////////////////////////////////////
  {0x4F,  0,  0,  { } },
  {0x50,  0,  0,  { } },
  {0x41,  0,  0,  { } },
  {0x42,  2,  1,  { {  0,     65.535,      GenericParser,   "(256*A+B)/1000",       "V",            0.1,0.1 }, } }, // Control module voltage
  {0x43,  2,  1,  { {  0,     24700,       GenericParser,   "(100/255)*(256*A+B)",  "%",            0.1,0.1 }, } }, // Absolute load value
  {0x44,  2,  1,  { {  0,     2,           GenericParser,   "(2/65536)*(256*A+B)",  "ratio",        0.1,0.1 }, } }, // Commanded Air-Fuel Equivalence Ratio {lambda,λ}
  {0x45,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Relative throttle position
  {0x46,  1,  1,  { { -40,    215,         GenericParser,   "A-40",                 "°C",           0.1,0.1 }, } }, // Ambient air temperature
  {0x47,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Absolute throttle position B
  {0x48,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Absolute throttle position C
  {0x49,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Accelerator pedal position D
  {0x4A,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Accelerator pedal position E
  {0x4B,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Accelerator pedal position F
  {0x4C,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Commanded throttle actuator
  {0x4D,  1,  1,  { {  0,     65535,       GenericParser,   "256*A+B",              "min",          0.1,0.1 }, } }, // Time run with MIL on
  {0x4E,  1,  1,  { {  0,     65535,       GenericParser,   "256*A+B",              "min",          0.1,0.1 }, } }, // Time since trouble codes cleared
  {0x52,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Ethanol fuel %
  {0x53,  2,  1,  { {  0,     327.675,     GenericParser,   "(256*A+B)/200",        "kPa",          0.1,0.1 }, } }, // Absolute Evap system Vapor Pressure
  {0x54,  2,  1,  { { -32768, 32767,       GenericParser,   "256*A+B",              "Pa",           0.1,0.1 }, } }, // Evap system vapor pressure !!!!!AB is two's complement signed!!!!!
  {0x55,  1,  2,  { { -100,   99.2,        GenericParser,   "(100/128)*A-100",      "%",            0.1,0.1 },  { -100,   99.2,       GenericParser,    "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Short term secondary oxygen sensor trim, A               : bank 1, B                                  : bank 3
  {0x56,  1,  2,  { { -100,   99.2,        GenericParser,   "(100/128)*A-100",      "%",            0.1,0.1 },  { -100,   99.2,       GenericParser,    "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Long term secondary oxygen sensor trim, A                : bank 1, B                                  : bank 3
  {0x57,  1,  2,  { { -100,   99.2,        GenericParser,   "(100/128)*A-100",      "%",            0.1,0.1 },  { -100,   99.2,       GenericParser,    "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Short term secondary oxygen sensor trim, A               : bank 2, B                                  : bank 4
  {0x58,  1,  2,  { { -100,   99.2,        GenericParser,   "(100/128)*A-100",      "%",            0.1,0.1 },  { -100,   99.2,       GenericParser,    "(100/128)*B-100",        "%",            0.1,0.1 },  } }, // Long term secondary oxygen sensor trim, A                : bank 2, B                                  : bank 4
  {0x59,  2,  1,  { {  0,     655350,      GenericParser,   "10*(256*A+B)",         "kPa",          0.1,0.1 }, } }, // Fuel rail absolute pressure
  {0x5A,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Relative accelerator pedal position
  {0x5B,  1,  1,  { {  0,     100,         GenericParser,   "(100/255)*A",          "%",            0.1,0.1 }, } }, // Hybrid battery pack remaining life
  {0x5C,  1,  1,  { { -40,    210,         GenericParser,   "A-40",                 "°C",           0.1,0.1 }, } }, // Engine oil temperature
  {0x5D,  2,  1,  { { -210,   301.992,     GenericParser,   "((256*A+B)/128)-210",  "°",            0.1,0.1 }, } }, // Fuel injection timing
  {0x5E,  2,  1,  { {  0,     3212.75,     GenericParser,   "(256*A+B)/20",         "L/h",          0.1,0.1 }, } }, // Engine fuel rate
  {0x5F,  0,  0,  { } }, // Emission requirements to which vehicle is designed

  {0x60,  0,  0,  { } }, // 0x60 {get supported PIDs}
  //////////////////////////////////////////////////
  // PID GROUP 4/6 {0x61-0x80}
  //////////////////////////////////////////////////
  {0x61,  1,  1,  { { -125,   130,         GenericParser,   "A-125",                "%",            0.1,0.1 }, } }, // Driver's demand engine - percent torque
  {0x62,  1,  1,  { { -125,   130,         GenericParser,   "A-125",                "%",            0.1,0.1 }, } }, // Actual engine - percent torque
  {0x63,  2,  1,  { {  0,     65535,       GenericParser,   "256*A+B",              "Nm",           0.1,0.1 }, } }, // Engine reference torque
  {0x64,  0,  0,  { } },
  {0x65,  0,  0,  { } },
  {0x66,  0,  0,  { } },
  {0x67,  0,  0,  { } },
  {0x68,  0,  0,  { } },
  {0x69,  0,  0,  { } },
  {0x6A,  0,  0,  { } },
  {0x6B,  0,  0,  { } },
  {0x6C,  0,  0,  { } },
  {0x6D,  0,  0,  { } },
  {0x6E,  0,  0,  { } },
  {0x6F,  0,  0,  { } },
  {0x70,  0,  0,  { } },
  {0x71,  0,  0,  { } },
  {0x72,  0,  0,  { } },
  {0x73,  0,  0,  { } },
  {0x74,  0,  0,  { } },
  {0x75,  0,  0,  { } },
  {0x76,  0,  0,  { } },
  {0x77,  0,  0,  { } },
  {0x78,  0,  0,  { } },
  {0x79,  0,  0,  { } },
  {0x7A,  0,  0,  { } },
  {0x7B,  0,  0,  { } },
  {0x7C,  0,  0,  { } },
  {0x7D,  0,  0,  { } },
  {0x7E,  0,  0,  { } },
  {0x7F,  0,  0,  { } },



  {0x80,  0,  0,  { } }, // {get supported PIDs}
  //////////////////////////////////////////////////
  // PID GROUP 5/7 {0x81-0xA0}
  //////////////////////////////////////////////////
  {0x81,  0,  0,  { } },
  {0x82,  0,  0,  { } },
  {0x83,  0,  0,  { } },
  {0x84,  0,  0,  { } },
  {0x85,  0,  0,  { } },
  {0x86,  0,  0,  { } },
  {0x87,  0,  0,  { } },
  {0x88,  0,  0,  { } },
  {0x89,  0,  0,  { } },
  {0x8A,  0,  0,  { } },
  {0x8B,  0,  0,  { } },
  {0x8C,  0,  0,  { } },
  {0x8D,  0,  0,  { } },
  {0x8E,  0,  0,  { } },
  {0x8F,  0,  0,  { } },
  {0x90,  0,  0,  { } },
  {0x91,  0,  0,  { } },
  {0x92,  0,  0,  { } },
  {0x93,  0,  0,  { } },
  {0x94,  0,  0,  { } },
  {0x95,  0,  0,  { } },
  {0x96,  0,  0,  { } },
  {0x97,  0,  0,  { } },
  {0x98,  0,  0,  { } },
  {0x99,  0,  0,  { } },
  {0x9A,  0,  0,  { } },
  {0x9B,  0,  0,  { } },
  {0x9C,  0,  0,  { } },
  {0x9D,  0,  0,  { } },
  {0x9E,  0,  0,  { } },
  {0x9F,  0,  0,  { } },

  {0xA0,  0,  0,  { } }, // {get supported PIDs}
  //////////////////////////////////////////////////
  // PID GROUP 6/7 {0xA1-0xC0}
  //////////////////////////////////////////////////
  {0xA1,  0,  0,  { } },
  {0xA2,  0,  0,  { } },
  {0xA3,  0,  0,  { } },
  {0xA4,  0,  0,  { } },
  {0xA5,  0,  0,  { } },
  {0xA6,  4,  1,  { {  0,     429496729.5, GenericParser,   "(A*(2^24)+B*(2^16)+C*(2^8)+D))/10","Nm", 0.1, 0.1}} }, // Odometer
  {0xA7,  0,  0,  { } },
  {0xA8,  0,  0,  { } },
  {0xA9,  0,  0,  { } },

  {0xC0,  0,  0,  { } }, // 0xC0 {get supported PIDs}
  //////////////////////////////////////////////////
  // PID GROUP 7/7 {0xC1-0xE0}
  //////////////////////////////////////////////////
  {0xC3,  0,  0,  { } },
  {0xC4,  0,  0,  { } },
};









PIDInfo getPIDInfoFromHexValue(unsigned char targetHexValue)
{
  for (uint64_t i = 0; i < sizeof(CURRENT_DATA_PIDS); i++)
  {
    PIDInfo pid;
    memcpy_P(&pid, &CURRENT_DATA_PIDS[i], sizeof(PIDInfo));
    if (pid.HexValue == targetHexValue)
    {
      return pid;
    }
  }
}

BitEncodingInfo getBitEncodedPIDInfo(unsigned char pidHexValue, unsigned char bitEncodedHexValue)
{
  for (uint64_t i = 0; i < sizeof(BIT_ENCODED_CURRENT_DATA_PIDS); i++)
  {
    BitEncodedPIDInfo pid;
    memcpy_P(&pid, &BIT_ENCODED_CURRENT_DATA_PIDS[i], sizeof(BitEncodedPIDInfo));
    if (pid.HexValue == pidHexValue)
    {
        for (uint64_t j = 0; j < sizeof(pid.Encodings); j++)
        {
          if (pid.Encodings[j].HexValue == bitEncodedHexValue)
          {
            return pid.Encodings[j];
          }
        }
    }
  }
}
