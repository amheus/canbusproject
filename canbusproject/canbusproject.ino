
#include "canbus_helpers.h"
#include "logging_helpers.h"
#include "ui_helpers.h"

#include <SparkFun_u-blox_GNSS_Arduino_Library.h>
#include <Adafruit_ISM330DHCX.h>

#include <Wire.h>
#include <SPI.h>

SFE_UBLOX_GNSS sparkfunGPSModule;
Adafruit_ISM330DHCX ism330dhcx;

RecordedSensorDataCradle CurrentSensorReadings;

bool timeSynced = false;

void setup()
{
  Serial.begin(115200);
  Wire.begin();
  sparkfunLCDModule.begin(Wire);
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  sparkfunLCDModule.setContrast(5);
  
  // pin config
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (1/7)", "Pinout", "", "");
  initPins();

  // CANBUS INIT
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (2/7)", "Canbus", "", "");
  // if(!Canbus.init(CANSPEED_125)) displayErrorMessage("CANBUS INIT FAIL");
  // if(!Canbus.init(CANSPEED_250)) displayErrorMessage("CANBUS INIT FAIL");
  if(!mcp2515_init(CANSPEED_500)) displayErrorMessage("CANBUS INIT FAIL");

  // SDCARD INIT
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (3/7)", "SD Card", "", "");
  digitalWrite(PIN_SD_CARD_CHIP_SELECT, HIGH);
  if (!SD.begin(PIN_SD_CARD_CHIP_SELECT)) displayFatalErrorMessage("    SD INIT FAIL");

  // GPS INIT
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (4/7)", "GPS", "", "");
  if (sparkfunGPSModule.begin() == false) displayErrorMessage("   GPS INIT FAIL");
  sparkfunGPSModule.setI2COutput(COM_TYPE_UBX);
  sparkfunGPSModule.saveConfigSelective(VAL_CFG_SUBSEC_IOPORT);

  // ACCELEROMETER INIT
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (5/7)", "Accelerometer", "", "");
  if (!ism330dhcx.begin_I2C())  displayErrorMessage("  GRYO INIT FAIL");
  ism330dhcx.configInt1(false, false, true);
  ism330dhcx.configInt2(false, true, false);

  // RTC INIT
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (6/7)", "RTC", "", "");
  if (!sparkfunRTCModule.begin()) displayErrorMessage("   RTC INIT FAIL");

  // SET RTC TIME
  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  displayTextOnLCD("Init...    (7/7)", "RTC Time Setting", "", "");
  if (!sparkfunRTCModule.setToCompilerTime()) displayErrorMessage("CANNOT GET GPS TIME");

  delay(CONFIG_INIT_STEP_DELAY);
  updateLCDBacklight(255, 255, 255);
  
  sparkfunRTCModule.updateTime();
  LogFileName = "NO_VIN_" + String(sparkfunRTCModule.getEpoch()) + ".txt";
}


void setRTCTimeToGPSTime()
{
  if (!timeSynced)
  {
    uint32_t us;
    uint32_t epoch;
    epoch = sparkfunGPSModule.getUnixEpoch(us);

    if(sparkfunGPSModule.getTimeFullyResolved() && sparkfunGPSModule.getTimeValid() && sparkfunGPSModule.getConfirmedTime())
    {
      sparkfunRTCModule.setEpoch(epoch);
      Serial.print("Time updated to ");
      Serial.println(epoch);
      timeSynced = true;
    }
  }
}


void getGPSReadings()
{
  CurrentSensorReadings.longitude = sparkfunGPSModule.getLatitude();
  CurrentSensorReadings.latitude = sparkfunGPSModule.getLongitude();
  CurrentSensorReadings.altitude = sparkfunGPSModule.getAltitude();
  CurrentSensorReadings.siv = sparkfunGPSModule.getSIV();
}
void getAccellerometerReadings()
{
  sensors_event_t accel;
  sensors_event_t gyro;
  sensors_event_t temp;
  ism330dhcx.getEvent(&accel, &gyro, &temp);
  // CurrentSensorReadings.temperature = temp;
  CurrentSensorReadings.acceleration_x = accel.acceleration.x;
  CurrentSensorReadings.acceleration_y = accel.acceleration.y;
  CurrentSensorReadings.acceleration_z = accel.acceleration.z;
  CurrentSensorReadings.gyro_x = accel.gyro.x;
  CurrentSensorReadings.gyro_y = accel.gyro.y;
  CurrentSensorReadings.gyro_z = accel.gyro.z;
}
void getVehicleData()
{

}




int currentScreen = 0;
void renderDisplay()
{
  switch(currentScreen)
  {
    case 0:
      displayTextOnLCD("Data Recorded:  ", String(totalDataWrittenToSDCard) + " bytes", "", "");
      break;
    case 1:
      displayTextOnLCD("Space left on SD", "??? Kb", "", "");
      break;
    case 2:
      displayTextOnLCD(String(CurrentSensorReadings.longitude), String(CurrentSensorReadings.latitude), "", "");
      break;
  }
  currentScreen++;
  if (currentScreen > 2) currentScreen = 0;
}
int lastTime = 0;
void mode__dataLogging()
{
  getAccellerometerReadings();
  getVehicleData();

  if (millis() - lastTime > 5000)
  {
    lastTime = millis();
    getGPSReadings();
    setRTCTimeToGPSTime();

    renderDisplay();
  }
}


int waitForJoystickInput()
{
  int counter_up = 0;
  int counter_right = 0;
  int counter_down = 0;
  int counter_left = 0;
  int counter_click = 0;
  uint8_t upperLimit = 75;
  while (true)
  {
    if(digitalRead(PIN_JOYSTICK_UP) == 0) counter_up++;
    if(digitalRead(PIN_JOYSTICK_RIGHT) == 0) counter_right++;
    if(digitalRead(PIN_JOYSTICK_DOWN) == 0) counter_down++;
    if(digitalRead(PIN_JOYSTICK_LEFT) == 0) counter_left++;
    if(digitalRead(PIN_JOYSTICK_CLICK) == 0) counter_click++;

    if(counter_up > upperLimit) return PIN_JOYSTICK_UP;
    if(counter_right > upperLimit) return PIN_JOYSTICK_RIGHT;
    if(counter_down > upperLimit) return PIN_JOYSTICK_DOWN;
    if(counter_left > upperLimit) return PIN_JOYSTICK_LEFT;
    if(counter_click > upperLimit) return PIN_JOYSTICK_CLICK;
    delay(1);
  }
}

void modeHandleExitLogic(bool success, String message)
{
  if(success)
  {
    updateLCDBacklight(0, 255, 0);
    displayTextOnLCD(message, "Click to exit.", "", "");
    while (true)
    {
      if (waitForJoystickInput() == PIN_JOYSTICK_CLICK)
      {
        return;
      }
    }
  }
  
  updateLCDBacklight(255, 0, 0);
  displayTextOnLCD("Failure", message, "", "");
  delay(5000);
  return;
}

void mode_logging_obd2Only()
{
  Serial.println("Getting supported PIDs");
  getSupportedPIDs();
  Serial.print("Running ");
  Serial.print(NUMBER_OF_SUPPORTED_PIDS);
  Serial.println(" PIDs");

  for (int i = 0; i < NUMBER_OF_SUPPORTED_PIDS; i++)
  {
    double* results = getStandardPIDResult(SUPPORTED_PIDS[i]);
  }
}
void mode_logging_sensorsOnly()
{
  double* results = getStandardPIDResult(0x0C);
}
void mode_logging_fullLogging()
{
  double* results = getStandardPIDResult(0x00);
}
void mode_logging_extractDTCs()
{
  modeHandleExitLogic(false, "Not implemented");
}
void mode_configuration_updateTime()
{
  uint32_t us;
  uint32_t epoch;
  epoch = sparkfunGPSModule.getUnixEpoch(us);

  if(!sparkfunGPSModule.getTimeFullyResolved()) { modeHandleExitLogic(false, "Cannot resolve"); return; }
  if(!sparkfunGPSModule.getTimeValid())         { modeHandleExitLogic(false, "Invalid time"); return; }
  if(!sparkfunGPSModule.getConfirmedTime())     { modeHandleExitLogic(false, "Cannot confirm"); return; }

  sparkfunRTCModule.setEpoch(epoch);
  Serial.print("Time updated to ");
  Serial.println(epoch);
  timeSynced = true;
  modeHandleExitLogic(true, "Time synced");
}
void mode_configuration_getSupportedPIDs()
{
}

int currentlyTargetedMenuIndex = 0;
int currentlyTargetedChildMenuIndex = 0;
bool currentlyInSubMenu = false;
int numberOfMenus = 3;
Menu menuTree[] = {
  {
    "Logging",
    {
      {"Full Logging", mode_logging_fullLogging},
      {"OBD2 Only", mode_logging_obd2Only},
      {"Sensors Only", mode_logging_sensorsOnly},
      {"Extract DTCs", mode_logging_extractDTCs},
    },
    4
  },
  {
    "Configuration",
    {
      {"Sync Time GPS>RTC", mode_configuration_updateTime},
      {"Get Supported PIDs", mode_configuration_getSupportedPIDs},
      {"Set Compass Offset", mode_configuration_getSupportedPIDs},
    },
    3
  },
  {
    "Self Tests",
    {
      {"Get GPS Location", mode_configuration_updateTime},
      {"Get Gyro", mode_configuration_updateTime},
      {"Get Accelleration", mode_configuration_updateTime},
    },
    3
  },
};
void loop()
{
  updateLCDBacklight(255, 255, 255);
  if(currentlyInSubMenu)
  {
    if (currentlyTargetedChildMenuIndex == 0)
    {
      displayTextOnLCD(
        "/" + menuTree[currentlyTargetedMenuIndex].title,
        '>' + menuTree[currentlyTargetedMenuIndex].items[currentlyTargetedChildMenuIndex].title,
        ' ' + menuTree[currentlyTargetedMenuIndex].items[currentlyTargetedChildMenuIndex+1].title,
        ' ' + menuTree[currentlyTargetedMenuIndex].items[currentlyTargetedChildMenuIndex+2].title
        );
    }
    else if (currentlyTargetedChildMenuIndex == menuTree[currentlyTargetedMenuIndex].itemCount - 1)
    {
      displayTextOnLCD(
        "/" + menuTree[currentlyTargetedMenuIndex].title,
        ' ' + menuTree[currentlyTargetedMenuIndex].items[currentlyTargetedChildMenuIndex-2].title,
        ' ' + menuTree[currentlyTargetedMenuIndex].items[currentlyTargetedChildMenuIndex-1].title,
        '>' + menuTree[currentlyTargetedMenuIndex].items[currentlyTargetedChildMenuIndex].title
        );
    }
    else 
    {
      displayTextOnLCD(
        "/" + menuTree[currentlyTargetedMenuIndex].title,
        ' ' + menuTree[currentlyTargetedMenuIndex].items[currentlyTargetedChildMenuIndex-1].title,
        '>' + menuTree[currentlyTargetedMenuIndex].items[currentlyTargetedChildMenuIndex].title,
        ' ' + menuTree[currentlyTargetedMenuIndex].items[currentlyTargetedChildMenuIndex+1].title
        );
    }

    int action = waitForJoystickInput();
    switch(action)
    {
      case PIN_JOYSTICK_UP:
        currentlyTargetedChildMenuIndex--;
        if (currentlyTargetedChildMenuIndex < 0) currentlyTargetedChildMenuIndex = 0;
        break;
      case PIN_JOYSTICK_DOWN:
        currentlyTargetedChildMenuIndex++;
        if (currentlyTargetedChildMenuIndex > menuTree[currentlyTargetedMenuIndex].itemCount - 1)
        currentlyTargetedChildMenuIndex = menuTree[currentlyTargetedMenuIndex].itemCount - 1;
        break;
      case PIN_JOYSTICK_LEFT:
        clearAllTextOnLCD();
        currentlyTargetedChildMenuIndex = 0;
        currentlyInSubMenu = false;
        break;
      case PIN_JOYSTICK_RIGHT:
        clearAllTextOnLCD();
        menuTree[currentlyTargetedMenuIndex].items[currentlyTargetedChildMenuIndex].action();
        break;
    }
  }
  else
  {
    if (currentlyTargetedMenuIndex == 0)
    {
      displayTextOnLCD(
        "/",
        '>' + menuTree[currentlyTargetedMenuIndex].title,
        ' ' + menuTree[currentlyTargetedMenuIndex+1].title,
        ' ' + menuTree[currentlyTargetedMenuIndex+2].title
        );
    }
    else if (currentlyTargetedMenuIndex == numberOfMenus - 1)
    {
      displayTextOnLCD(
        "/",
        ' ' + menuTree[currentlyTargetedMenuIndex-2].title,
        ' ' + menuTree[currentlyTargetedMenuIndex-1].title,
        '>' + menuTree[currentlyTargetedMenuIndex].title
        );
    }
    else 
    {
      displayTextOnLCD(
        "/",
        ' ' + menuTree[currentlyTargetedMenuIndex - 1].title,
        '>' + menuTree[currentlyTargetedMenuIndex].title,
        ' ' + menuTree[currentlyTargetedMenuIndex + 1].title
        );
    }

    int action = waitForJoystickInput();
    switch(action)
    {
      case PIN_JOYSTICK_UP:
        currentlyTargetedMenuIndex--;
        if (currentlyTargetedMenuIndex < 0) currentlyTargetedMenuIndex = 0;
        break;
      case PIN_JOYSTICK_DOWN:
        currentlyTargetedMenuIndex++;
        if (currentlyTargetedMenuIndex > numberOfMenus-1) currentlyTargetedMenuIndex = numberOfMenus-1;
        break;
      case PIN_JOYSTICK_RIGHT:
        clearAllTextOnLCD();
        currentlyInSubMenu = true;
        break;
    }
  }
}
