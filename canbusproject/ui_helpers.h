
#include <SerLCD.h>
SerLCD sparkfunLCDModule;

int currentBacklightRed,currentBacklightGreen,currentBacklightBlue = -1;
String currentText[4];

void updateLCDBacklight(int r, int g, int b)
{
  if (r == currentBacklightRed && g == currentBacklightGreen && b == currentBacklightBlue) return;
  currentBacklightRed = r;
  currentBacklightGreen = g;
  currentBacklightBlue = b;
  sparkfunLCDModule.setBacklight(r, g, b);
  delay(250);
}


String padRight(String targetString)
{
  for(int i = 0; i < (20 - targetString.length()); i++)
  {
    targetString += ' ';  
  }
  return targetString;
}

void displayTextOnLCD(String line1, String line2, String line3, String line4)
{
  if (currentText[0] != line1)
  {
    sparkfunLCDModule.setCursor(0, 0);
    sparkfunLCDModule.print(padRight(line1));
    currentText[0] = line1;
  }
  if (currentText[1] != line2)
  { 
    sparkfunLCDModule.setCursor(0, 1);
    sparkfunLCDModule.print(padRight(line2));
    currentText[1] = line2;
  }
  if (currentText[2] != line3)
  {
    sparkfunLCDModule.setCursor(0, 2);
    sparkfunLCDModule.print(padRight(line3));
    currentText[2] = line3;
  }
  if (currentText[3] != line4)
  {
    sparkfunLCDModule.setCursor(0, 3);
    sparkfunLCDModule.print(padRight(line4));
    currentText[3] = line4;
  }
}

void clearAllTextOnLCD()
{
  sparkfunLCDModule.clear();
}


void displayFatalErrorMessage(String message)
{
  updateLCDBacklight(255, 0, 0);
  displayTextOnLCD("  FATAL ERROR!  ", message, "", "");

  while (true)
  {
    // digitalWrite(PIN_LIGHT_CANBUS_LEFT, HIGH);
    // digitalWrite(PIN_LIGHT_CANBUS_RIGHT, LOW);
    delay(500);
    // digitalWrite(PIN_LIGHT_CANBUS_LEFT, LOW);
    // digitalWrite(PIN_LIGHT_CANBUS_RIGHT, HIGH);
    delay(500);
  }
}

void displayErrorMessage(String message)
{
  updateLCDBacklight(255, 140, 0);
  displayTextOnLCD("     ERROR!     ", message, "", "");
  delay(2500);
}

void displaySuccessMessage(String message)
{
  updateLCDBacklight(0, 255, 0);
  displayTextOnLCD(message, message, "", "");
  delay(2500);
}
