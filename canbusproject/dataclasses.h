
struct PIDParameters {
  double MinimumValue;
  double MaximumValue;
  double (*Parser)(const char*, long[]);
  const char* Formula;
  const char* UnitOfMeasurement;
  double VariationDown;
  double VariationUp;
};

struct PIDInfo {
  unsigned char HexValue;
  unsigned NumberOfBytes;
  int NumberOfParameters;
  PIDParameters Parameters[2];
};

struct BitEncodingInfo {
  unsigned char HexValue;
  char ShortDescription[20];
};

struct BitEncodedPIDInfo {
  unsigned char HexValue;
  BitEncodingInfo Encodings[0xFF];
};

struct tCANWithSuccess {
  tCAN CANFrame;
  bool Success;
};



struct RecordedSensorDataCradle {
    long longitude;
    long latitude;
    long altitude;
    int siv;

    int temperature;
    int acceleration_x;
    int acceleration_y;
    int acceleration_z;
    int gyro_x;
    int gyro_y;
    int gyro_z;
};



const int MAX_MENUS = 5;
const int MAX_ITEMS_PER_MENU = 5;
struct MenuItem {
  String title;
  void (*action)();
};
struct Menu {
  String title;
  MenuItem items[MAX_ITEMS_PER_MENU];
  int itemCount;
};






class VIN {
  public:
    char CountryOfAssembly;
    char Manufacturer[3];
    char BodyStyleAndModelAndEngineType[5];
    char SecurityCode;
    char VehicleYear;
    char PlantBuilt;
    char SerialNumber[6];

    VIN(char vin[17])
    {
      CountryOfAssembly = vin[0];
      strncpy(Manufacturer, vin + 1, 2);
      strncpy(BodyStyleAndModelAndEngineType, vin + 3, 5);
      SecurityCode = vin[8];
      VehicleYear = vin[9];
      PlantBuilt = vin[10];
      strncpy(SerialNumber, vin + 11, 6);
    }
};
