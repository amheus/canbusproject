// #include <Canbus.h>
#include "mcp2515.h"
#include "mcp2515_defs.h"

#define PID_REQUEST     0x7DF
#define PID_REPLY       0x7E8
#define CANSPEED_125    7   // CAN speed at 125 kbps
#define CANSPEED_250    3   // CAN speed at 250 kbps
#define CANSPEED_500    1   // CAN speed at 500 kbps

#include "pid_wrapper.h"

unsigned char SUPPORTED_PIDS[175];
int NUMBER_OF_SUPPORTED_PIDS = 0;

int numberOfAttemptsBeforeGivingUp = 2;
int delayBetweenAttempts = 500;
bool canMessageFailureFlag = false;

int numberOfCANBusChecksBeforeGivingUp = 10;


tCANWithSuccess goGet(unsigned char serviceID, unsigned char parameterID)
{
  canMessageFailureFlag = false;
  tCAN message;
  message.id = PID_REQUEST;
  message.header.rtr = 0;
  message.header.length = 8;
  message.data[0] = 0x02;
  message.data[1] = 0x01;
  message.data[2] = parameterID;
  message.data[3] = 0x00;
  message.data[4] = 0x00;
  message.data[5] = 0x00;
  message.data[6] = 0x00;
  message.data[7] = 0x00;


  mcp2515_bit_modify(CANCTRL, (1 << REQOP2) | (1 << REQOP1) | (1 << REQOP0), 0);

  if (!mcp2515_send_message(&message))
  {
    canMessageFailureFlag = true;
    Serial.println("SEND FAILURE");
    return {message, false};
  }

  int counter = 0;
  delay(10);
  while(!mcp2515_check_message())
  {
    delay(5);
    counter++;
    if (counter > numberOfCANBusChecksBeforeGivingUp)
    {
      canMessageFailureFlag = true;
      Serial.println("CHECK FAILURE");
      return {message, false};
    }
  }
  
  if (!mcp2515_get_message(&message))
  {
    canMessageFailureFlag = true;
    Serial.println("GET FAILURE");
    return {message, false};
  }
    return {message, true};
}

double* getStandardPIDResult(unsigned char parameterID)
{
  PIDInfo targetPID = getPIDInfoFromHexValue(parameterID);
  tCANWithSuccess responseMessage = goGet(0x01, targetPID.HexValue);

  if(canMessageFailureFlag) return;

  unsigned long vars[] = {
    responseMessage.CANFrame.data[3],
    responseMessage.CANFrame.data[4],
    responseMessage.CANFrame.data[5],
    responseMessage.CANFrame.data[6],
  };

  Serial.print(String(parameterID, HEX));
  Serial.print("==");
  Serial.print(String(responseMessage.CANFrame.data[2], HEX));
  Serial.print(" ==> ");
  Serial.print(String(vars[0], HEX));
  Serial.print(" ");
  Serial.print(String(vars[1], HEX));
  Serial.print(" ");
  Serial.print(String(vars[2], HEX));
  Serial.print(" ");
  Serial.print(String(vars[3], HEX));
  Serial.print(" ==> ");

  double results[2];

  results[0] = targetPID.Parameters[0].Parser(targetPID.Parameters[0].Formula, vars);
  if (targetPID.NumberOfParameters == 2)
  {
    results[1] = targetPID.Parameters[1].Parser(targetPID.Parameters[0].Formula, vars);
  }
  Serial.println(results[0]);
  return results;
}


void processSupportedPidsResponse(tCANWithSuccess response, int range_start, int range_end)
{
  String binaryString = "";
  for (int i = 3; i <= 6; i++)
  {
    Serial.print(String(response.CANFrame.data[i], HEX));
    Serial.print(" ");
    uint8_t dataByte = response.CANFrame.data[i];
    for (int j = 7; j >= 0; j--)
    {
      binaryString += ((dataByte >> j) & 0x01) ? '1' : '0';
    }
  }

  Serial.println(" ");
  Serial.println(binaryString);

  for (int i = range_start; i < range_end; i++)
  {
    if (binaryString.charAt(i - range_start) == '1')
    {
      PIDInfo temp = getPIDInfoFromHexValue(i);
      if (temp.NumberOfParameters > 0)
      {
        SUPPORTED_PIDS[NUMBER_OF_SUPPORTED_PIDS] = temp.HexValue;
        NUMBER_OF_SUPPORTED_PIDS++;
      }
    }
  }
}



void getSupportedPIDs()
{
  NUMBER_OF_SUPPORTED_PIDS = 0;
  Serial.println("----------");  
  processSupportedPidsResponse(goGet(0x01, 0x00), 0x01, 0x20);
  Serial.println("");
  processSupportedPidsResponse(goGet(0x01, 0x20), 0x21, 0x40);
  Serial.println("");
  processSupportedPidsResponse(goGet(0x01, 0x40), 0x41, 0x60);
  Serial.println("");
  processSupportedPidsResponse(goGet(0x01, 0x60), 0x61, 0x80);
  Serial.println("");
  processSupportedPidsResponse(goGet(0x01, 0x80), 0x81, 0xA0);
  Serial.println("");
  processSupportedPidsResponse(goGet(0x01, 0xA0), 0xA1, 0xC0);
  Serial.println("");
  processSupportedPidsResponse(goGet(0x01, 0xC0), 0xC1, 0xE0);
  Serial.println("-----");
  for (int i = 0; i < NUMBER_OF_SUPPORTED_PIDS; i ++)
  {
    Serial.println(String(SUPPORTED_PIDS[i], HEX));
  }
  
  Serial.println("----------");
}
