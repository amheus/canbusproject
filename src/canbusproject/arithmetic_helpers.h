
#include "tinyexpr.h"

double evaluateFormula(const char* algebraicFormula, long values[])
{
  char valuePlaceholders[26];
  int numValues = sizeof(values);
  for (int i = 0; i < numValues; i++)
  {
    valuePlaceholders[i] = char(65 + i);
  }
  String formulaCopy = algebraicFormula;

  for (int i = 0; i < numValues; i++)
  {
    char var = valuePlaceholders[i];
    char valStr[10];
    dtostrf(values[i], 1, 6, valStr);

    char varStr[2];
    varStr[0] = var;
    varStr[1] = '\0';

    formulaCopy.replace(varStr, valStr);
  }
  
  int error;
  double result = te_interp(formulaCopy.c_str(), &error);

  if (error)
  {
    Serial.print("Expression evaluation error");
    return 0.0;
  }

  return result;
}
