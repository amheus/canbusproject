
/*
#define	P_MOSI	        B,3
#define	P_MISO	        B,4
#define	P_SCK	          B,5
//#define	MCP2515_CS			D,3	// Rev A
#define	MCP2515_CS			B,2 // Rev B
#define	MCP2515_INT			D,2
#define LED2_HIGH			  B,0
#define LED2_LOW			  B,0
*/
#define	P_MOSI	        B,2 // 51
#define	P_MISO	        B,3 // 50
#define	P_SCK	          B,1 // 52
#define	MCP2515_CS			B,0 // Rev 53
#define	MCP2515_INT			E,4 // 2
#define LED2_HIGH       H,5 // pin 8
#define LED2_LOW        H,5 // pin 8

#define PIN_BUZZER 30
#define PIN_SD_CARD_CHIP_SELECT 9
#define PIN_LIGHT_BUILTIN 6
#define PIN_LIGHT_CANBUS_LEFT 8
#define PIN_LIGHT_CANBUS_RIGHT 7

/*
#define PIN_JOYSTICK_UP A1
#define PIN_JOYSTICK_RIGHT A9
#define PIN_JOYSTICK_DOWN A3
#define PIN_JOYSTICK_LEFT A2
*/
#define PIN_JOYSTICK_UP A2
#define PIN_JOYSTICK_RIGHT A1
#define PIN_JOYSTICK_DOWN A9
#define PIN_JOYSTICK_LEFT A3

#define PIN_JOYSTICK_CLICK A8

#define CONFIG_INIT_STEP_DELAY 150


void initPins()
{
  pinMode(PIN_SD_CARD_CHIP_SELECT, OUTPUT);
  pinMode(PIN_BUZZER, OUTPUT);
  pinMode(PIN_LIGHT_CANBUS_RIGHT, OUTPUT);
  pinMode(PIN_LIGHT_CANBUS_RIGHT, OUTPUT);
  // pinMode(PIN_LIGHT_DATA_LOG_ALERT, OUTPUT);
  
  pinMode(PIN_JOYSTICK_UP, INPUT);
  pinMode(PIN_JOYSTICK_RIGHT, INPUT);
  pinMode(PIN_JOYSTICK_DOWN, INPUT);
  pinMode(PIN_JOYSTICK_LEFT, INPUT);
  pinMode(PIN_JOYSTICK_CLICK, INPUT);
  digitalWrite(PIN_JOYSTICK_UP, HIGH);
  digitalWrite(PIN_JOYSTICK_RIGHT, HIGH);
  digitalWrite(PIN_JOYSTICK_DOWN, HIGH);
  digitalWrite(PIN_JOYSTICK_LEFT, HIGH);
  digitalWrite(PIN_JOYSTICK_CLICK, HIGH);
}
