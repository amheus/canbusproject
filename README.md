
# Arduino
## Required Packages
### From Library Manager
- `SD` (1.2.4)
- `SparkFun Qwiic RTC RV8803 Arduino Library` (1.2.8)
- `SparkFun SerLCD Arduino Library` (1.0.09)
- `SparkFun u-blox GNSS Arduino Library` (2.2.24)
- `Adafruit LSM6DS` (4.7.0)
- `Adafruit BusIO` (1.14.4)
- `Adafruit Unified Sensor` (1.1.12)
